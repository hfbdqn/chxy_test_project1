package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SuleiController {
  
    @GetMapping("/sulei")
    public String hello(){
        return "sulei";
    }
}
