package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;


@RestController
public class SongjieController {
    @Autowired
    BeiwangRepository beiwangRepository;

    @GetMapping("/songjie")
    public String hello() {
		return "Songjie";
    }
}
