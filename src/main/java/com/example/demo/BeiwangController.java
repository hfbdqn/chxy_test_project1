package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class BeiwangController {
    @Autowired
    BeiwangRepository beiwangRepository;

    @GetMapping("/add_beiwang")
    public void addBeiwang(BeiwangZhangzheng
                  beiwangZhangzheng){
        beiwangZhangzheng.setDate(new Date());
        beiwangRepository.save(beiwangZhangzheng);
    }
}
